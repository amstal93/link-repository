import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-anonymus',
  templateUrl: './anonymus.component.html',
  styleUrls: ['./anonymus.component.scss']
})
export class AnonymusComponent implements OnInit {

  constructor(public userService: UserService) { }

  ngOnInit(): void {
  }

}
