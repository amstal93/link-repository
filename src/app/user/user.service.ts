import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bearer, Register } from './user';
import { Observable, of, from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

const jwtHelper = new JwtHelperService();

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private hostname = `${environment.api}/auth/`;

  public isAuth: boolean;
  public token: Bearer;

  constructor(private http: HttpClient, private cookieService: CookieService, private router: Router) {
    this.checkAuthenticated();
  }

  checkAuthenticated(): Observable<boolean> {
    if (
      this.token !== undefined &&
      this.token !== null &&
      !jwtHelper.isTokenExpired(this.token.access_token)
    ) {
      this.isAuth = true;
      return of(true);
    } else {
      this.token = new Bearer();
      if (this.cookieService.check('Refresh')) {
        this.token.access_token = this.cookieService.get('Token');
        this.token.username = this.cookieService.get('Username');
        return from(this.refresh(this.cookieService.get('Refresh')));
      } else {
        this.isAuth = false;
        return of(false);
      }
    }
  }

  /**
   * login
   */
  public login(email: string, password: string): Observable<Bearer> {
    const body = {
      username: email,
      password,
    };
    const obs = this.http.post<Bearer>(this.hostname + 'login', body);
    obs.subscribe((x) => {
      this.token = x;
      this.cookieService.set('Username', this.token.username);
      this.cookieService.set('Token', this.token.access_token);
      this.cookieService.set('Refresh', this.token.refresh_token);
      this.isAuth = true;
    });
    return obs;
  }

  /**
   * refresh
   */
  // tslint:disable-next-line: variable-name
  public async refresh(refresh_token: string): Promise<boolean> {
    const body = {
      refresh_token,
    };
    this.isAuth = false;
    const obs = this.http.post<Bearer>(this.hostname + 'refresh', body);
    await obs.toPromise().then((result) => {
      this.token = result;
      this.cookieService.set('Username', this.token.username);
      this.cookieService.set('Token', this.token.access_token);
      this.isAuth = true;
    });
    return this.isAuth;
  }

  /**
   * register
   */
  public register(
    email: string,
    username: string,
    password: string
  ): Observable<Register> {
    const body = {
      email,
      username,
      password,
    };
    return this.http.post<Register>(this.hostname + 'register', body);
  }

  /**
   * confirm
   */
  public confirm(username: string, code: string): Observable<any> {
    const body = {
      username,
      code,
    };
    return this.http.post<any>(this.hostname + 'confirm', body);
  }

  /**
   * logout
   */
  public logout() {
    this.token = null;
    this.isAuth = false;
    this.cookieService.deleteAll();
    this.router.navigate(["/"])
    //window.location.href = environment.url;
  }

  /**
   * manage
   */
  // tslint:disable-next-line: variable-name
  public manage(old_Password: string, new_Password: string): Observable<any> {
    const body = {
      access_token: this.token.access_token,
      old_Password,
      new_Password,
    };
    return this.http.post<any>(this.hostname + 'change_password', body);
  }

  /**
   * reset
   */
  public reset(username: string): Observable<any> {
    const body = {
      username,
    };
    return this.http.post<any>(this.hostname + 'forgot_password', body);
  }

  /**
   * reset
   */
  public setPassword(
    username: string,
    // tslint:disable-next-line: variable-name
    new_password: string,
    code: string
  ): Observable<any> {
    const body = {
      username,
      new_password,
      code,
    };
    return this.http.post<any>(this.hostname + 'confirm_forgot_password', body);
  }
}
